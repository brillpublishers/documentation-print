# Requesting a Cover

## Cover Request Form

Get this form via: Excel: File > New > My Templates > Klopotek > Cover Request.
It is automatically filled in via Klopotek. 

If you cannot access this form, make your own. You need the information specified below.

## Necessary Information

Project number;

Title,

Subtitle,

Author(s),

Editor(s),

Volume number;

Imprint: (whether it is Brill, or Brill|Rodopi, etc.),

Binding: Hardback or Paperback (Hardback is standard),

Trim size: format of book,

Spine width: Offset books have 1, POD books have two spine widths. Bookmasters is for the American market (is printed there), Printforce for the rest.

Documents for the standard trim sizes and spine calculators are found at [Brillnet Book Documents](http://brillnet/book-documents).

ISBN;

Blurb: this is either the long or the short description in Klopotek

Biographical text: of both author(s) and editor(s)


## Requesting the Cover

We have a pool of cover designers, at [Brillnet CDE](http://brillnet/CDE-books/central-desk-editing-books) a list of which designer is assigned to which series.

You need both a cover.pdf, and a cover image.jpeg. The latter is for advertising goals such as the image at brill.com.

Note that for POD books you need two covers because you have two different spine widths (US & other): those are called _cmyk.pdf and uspod.pdf

Obviously, the cover needs some sort of design. For subsequent volumes, the design is already set by volume 1. Send this cover along with your request. (The cover designer will also stay the same of course.)

For a new book (series) you can give the designer some imput, inspired by other Brill books or your own imagination. Please do take in mind the Brill style, which the cover designer is also familiar with.

## Proofs

The first proof needs to be send to the Assistant Editor for approval. Afterwards, it needs to be send to the editors for approval.
A second proof can be made if necessary.
