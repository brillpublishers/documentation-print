# Proof 1

## Manuscript
- send each article to the typesetters separately 
- so each article gets its own pdf
- includes Table of Contents
- includes prelims, see CIP
- preferably also includes other pages such as Introduction and Abbreviations List
- request .txt file of whole manuscript for CIP

## Cover
- required: 'long description' (blurb), 'bibliographical note' (biography) from the Content tab in Klopotek.
- fill in this form in Excel: File > New > My Templates > Klopotek > Cover Request; it is automatically filled in with the information from Klopotek 
- in case of multiple editors, biographies can also be provided in a separate document
- request first proof of cover
- if working on subsequent volume, check design of first volume
- see for preferred cover designer G:\departments\Operational Process Management\Desk Editing\0Books\Cover designers book series
- check title and author's and editors' names in the fom
- send proof 1 to the assistant editor for approval, and when approved
- send to the editors for approval
- request front cover jpeg (for on brill.com) and send to covers@brill.com

## CIP
- request CIP if not yet available
For series, only the first volume needs a CIP requested, subsequent volumes will be added to the CIP by LOC automatically. *<check>*
- required: .txt file of ToC, prelims, and (significant part of) content
- see [CIP](CIP.md) for more information

## Prelims
These are page i-iv of the book.
- Fill in this form in Excel: File > New > My Templates > Klopotek > Prelim Copy Form; it is automatically filled in with the information from Klopotek.
- check carefully and add additional information if necessary 
- p. ii: include a model page from the most recent previous volume, mark changes to names or editorial board.
- p. iv: check ISSN
- in case of cover illustration, the credit line should be put on p. iv.

**Please note:** Freelancers do not have access to Klopotek. You can ask the Assistant Editor to fill out the forms for you.

