# Differences between normal book production and MRW book production

There are considerable differences in the tasks each person has between normal book production and book production for MRW.

| Normal Book Production | MRW Book Production |
| --- | --- |
| **Aquisition Editor** | **Aquisition Editor** |
| takes on project | idem |
| **Assistant Editor** | **Assitant Editor** |
| everything up to first proof, | unclear, can help with Klopotek and certain forms (calculation) |
| delivery of files by author | **Project Manager** |
| delivers to CDE | projectmanagement, |
| **Desk Editor** | workflow
| proofs and checking | idem |
| delivery to production | idem |
| **Production** | **Production** |
| Send book for print | idem |

In short, for MRW the project manager takes on the tasks of both Assistant Editor and Desk Editor.

The project manager thus follows the whole process from delivery of the Word files by the editors till the final proof.
Moreover, the PM usually also send out the initial contracts, style guides, welcoming emails etc..