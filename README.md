# documentation-print

Information on how to publish a print book. 

This information was gathered by [Merel Oudshoorn](https://gitlab.com/m-oudshoorn) during the book projects _BEB_ II-IV and _SGG_. 

It is a work in progress, which can benefit from the insights you learned yourself.

Any additional information on the publishing of MRW print books can be sent to Ellen Girmscheid, <code>girmscheid at brill.com</code>, as well as any questions or remarks you might have.
