# Proof 2

This proof makes the project from separate articles to one manuscript.

## Manuscript, part 1

-  send the separate article files of proof 1 to the authors and editors for correction
- it is advisable to check each article for any obvious typesetting errors before sending
- the preferable workflow is: AU corrections > to Brill > ED corrections
Why?
  + author goes first, so editor can correct them
  + the *same* file with author corrections goes to editor, so you end up with 1 file per article
  + the authors send their file to Brill so you can check receipt and keep an eye on workflow
- please note that it is possible that several editors want to check the file, make sure the file doesn't end up getting several versions

### Time span:

check of articles upon receival proof 1: 1-2 weeks, depending on quantity

AU corrections: 1 month? 

ED corrections: 1 month?

## Manuscript, part 2

- check if all articles are received and indeed corrected
- request proof 2 of typesetter
  + this includes Table of Contents, prelims, CIP if already available, and others such as Abbreviations List.

### Time span:

proof 2 from typesetter: ??? (CDE gives 2 weeks, but longer?)

## Cover

- send cover for corrections if necessary
- send editor proof 2 of the cover if s/he requested it

## CIP

- if received: check
- send to typesetter to include in proof

