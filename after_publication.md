# It's Published!

First, celebrate the publication of your project. Well done! :)

## Editors

Inform editors when the book is going to be published, and when it is published.

The rest will be dealt with by either the Acquisition Editor or the Assistant Editor, or marketing.
That is:
- telling the world the book is published (marketing)
- giving editors a copy of the book, if that was consented (acq. and assistant editor)
- etc.

It might be, that editors will get a small amount of money for their work. Please check what was consented.
If so, you have to sort this out. (more info later)

## Authors

All authors will receive a digital offprint (pdf) of their contribution. Request secured digital offprints at the typesetter, and make sure the pdf is exactly as the article in the published book.
See information on the digital offprint [here](http://brillnet/sites/default/files/departments/central%20desk%20editing/Books/Procedure%20Digital%20Offprints.pdf)

Draft an email:
- the book is published
- thank you for your contribution
- please find attached the offprint
- please find attached the copyright letter of your article (can be found [here](http://brillnet/sites/default/files/departments/central%20desk%20editing/Books/Letter%20Digital%20Offprints_Edited%20Volume.pdf) )
- please fill in this form to receive your remuneration (if applicable)
- tell about author discount (if applicable, see below)

Sometimes, authors get a small remuneration for their contribution, relative to the word count. Please check what was consented, and if changes in word count mean changes in remuneration.
If applicable, attach a form to the email delivering the offprint, asking them for their bank account data. 
Form can be found here: (to do)

It might be wise, to check with the editors if some authors need additional remarks in this email. For example, if major changes have been committed by the editors to the article. 
In general, editors who have edited so extensively that their names (or "the editors") are cited as an author do not get any remuneration for this. 

### Author Discount

Usually, Brill authors get a discount of 35% on all Brill publications they would like to order in the future. (Check in the agreement if this is applicable to your authors.)

Please note that MRW authors cannot get this discount via the website, since they are not cited in Klopotek as author (usually, only the editors are cited there). 
If they would like to use the discount to order a book, let them email you their phonenumber, address and the name and isbn of the book they like to order. 

Use this data to fill in the Author Order Form *yourself* (DO NOT let the authors fill in this form, there is information in it they do not understand and it contains turpin email addresses, to which the authors might email themselves inadvertently).
Send the Author Order Form to turpin, for North America BrillNa@, for the rest of the world just Brill@ .

The Author Order Form can be found here: G:\General\Sjablonen\Marketing & Sales\Books

! Please note: it could be the case that the Assistant Editor takes care of this matter, please consult with him/her who will take upon them this task.  

