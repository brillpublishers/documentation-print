Proof 1:
    Manuscript, includes
        - Table of Contents
        - prelims (see below)
        - each lemma in separate file (.pdf) 
                to send to AU and ED for corrections
        
    Cover
        - see cover request form (automatically filled in from klopotek)
        - need: long description (blurb) and biographical note from Klopotek
        - send for approval to 1) assistant editor, 2) editors
        - request front_cover.jpg and send to covers@brill.com
        
    CIP
        - request if necessary
                necessary information: .txt file of ToC, prelims, content (or representative sample of content)
        - for subsequent volumes, information will be added to CIP by LOC
        - see document CIP for more information
        
    Prelims
        - see Prelim Copy Form (filled in automatically from Klopotek)
        - check form carefully and add information manually if necessary
        - p. ii : 
            include sample of p. ii from previous volumes
            mark changes to editor's names and editorial advisory board
            
            
Proof 2: 