# Schedule Overview

This overview provides the three basic steps in the proofing phase. 

Especially during Proof 2 it may occur that you need several rounds of corrections, considering the fact that MRW books are large undertakings, dealing with multiple authors (and thus, multiple styles) and several editors.

Proof 1 is the stage at which most things need to set in motion. Please think about several issues at an early stage to prevent delays and chaos later on: Introduction, Table of Contents, List of Contributors and List of Editors, Abbreviation Lists, and CIP data. (So, all stuff apart from the articles themselves.)

## Proof 1

At this stage, each article has its own pdf. 
This stage is meant for author corrections, and thorough proofreading by the editors.

At this stage, CDE normally requests the CIP data. CIP data can take up to 3 months *<check>* to be received. 

Request the cover.

Start making the prelims (page i-iv) and the Table of Contents. 
The Table of Contents needs to be made by the editors, who have to decide on the order of the articles. 

Moreover, pages such as an Introduction and Abbreviations List need to be made at this stage.

### time span: 2 months *<check>*
Proof 1 by typesetter: *<tba>*

Check if all articles are delivered and no obvious mistakes are made

Author and editor proofreading: 1 month

## Proof 2

At this stage, the proof becomes one pdf.
This stage is meant for checking by the editors.

Also, bear in mind that issues with need to be conform throughout all articles, can be checked at this stage. 

Send cover for corrections. Calculate spine width (preliminary) for cover. 
Check if spine width is not too big.

### time span: *<check>*
Proof 2 by typesetters: *<tba>*

Check the proof yourself: 1 week

Editor check: *<a week per editor?>*

## Proof 3

This is the final proof. 
Only obvious and huge errors are to be corrected.

In principle, editors do not get to see this proof.

Check if spine width is still correct. If not, request adjusted cover.

Prepare for delivery to production.

### time span: 2 weeks max.
Proof 3 by typesetters: 1 week

Check yourself: 1 day

Prepare for production

