# CIP

CIP data only needs to be requested for the first volume.
Please note that not all types of books get a CIP (yearbooks for instance are omitted). 

For the CIP you need a PDF of the Table of Contents, the prelims, and the content. 
If not all content is ready, you can send a representative portion of the content. This is necessary for the production of keywords.

The files need to be named as follows: isbn_surname-chief-editor_<proof 1 or text or whatever>.pdf

CIP data is requested by Mireille: pijl@brill.com.
Email her the directory of the files and other necessary information.

CIP data is delivered usually after 10 ten days, but may take up to 3 months. 

For more information see Book Procedure of CDE.
Please note that the procedure for requesting a CIP is changed recently, some information may be dated.

## Correcting CIP data

When you receive your CIP data (an email with a link), please check all data.
If corrections are necessary, send Mireille an email with a corrected pdf, emphasizing in the email what needs to be corrected.
