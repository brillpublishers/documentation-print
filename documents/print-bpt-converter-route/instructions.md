# Print: BPT-converter route

This document seeks to elaborate on the use, the _raison d'être_, and the history of the conversion of [BPT](https://brillpublishers.gitlab.io/documentation-brill-plain-text/) to PDF used for both typesetting and preview of works edited.

Please don't hesitate [to file an issue in this gitlab repository](https://gitlab.com/brillpublishers/documentation-print/-/issues/new "File an issue now") if anything remains unclear or you are stuck while using the BPT-converter to create PDFs.

## Getting started

### Installation
At the moment, executing a conversion via the [BPT-converter][1] does require quite a bit of command line work. (The [BPT-converter][1] was build to be invoked automatically by a push to a repository, which explains its developer-use orientation.) Please be aware of this. It is however doable with a bit of patience, I will go through everything required.

As explained above, the conversion from BPT to PDF goes via TeX. This means there are two major steps in the conversion. From BPT to TeX, and from TeX to PDF.

Steps:
1. Install [git](https://git-scm.com/downloads), [python](https://www.python.org/downloads/) (for a proper install using brew see [this link](https://www.freecodecamp.org/news/python-version-on-mac-update/)), [pip](https://pip.pypa.io/en/stable/installation/) (which is probably included with python), venv (see e.g. [this blog](https://realpython.com/python-virtual-environments-a-primer/#create-it)) if you do not already have them installed. (If you are on mac I recommend using [homebrew](https://brew.sh/) to install these.)

2. Clone the [BPT-converter repository][1] to your computer in a folder of your preference.
```
$ cd folder/you/want/your/repositories/in
$ git clone https://gitlab.com/brillpublishers/code/bpt-converter.git
```

3. Move into the repository folder and checkout the 'print' branch.
```
$ cd bpt-converter
$ git checkout print
```

4. Install the BPT-converter (on the print branch!) into a virtual environment. The `-e` flag makes sure you do not have to reinstall the converter if you edit the templates for example.
```
$ python -m venv venv
$ source venv/bin/activate
$ pip install -e .
```

5. Install [MacTeX](https://www.tug.org/mactex/) for mac or [TeX Live](https://www.tug.org/texlive/) for Windows or Linux. You can also get it through homebrew, your own distribution or package manager.

6. Open TeX Live Utility and make sure you get Lualatex installed and updated. (Both steps 7 and 8 can take a while. So be prepared.)

### Converting BPT to PDF

6. You now have the BPT-converter installed with the option to convert to .tex! Now you can use it like so:
```
$ bptconverter --source /path/to/input/folder --output path/to/output/folder --format tex
```

Note that you will have to activate the virtual environment before you start to work on some conversions.

7. You will now have converted the BPT-files in the input folder to `.tex`-files. You will find them in the defined output folder.

If you open up one of the `.tex` files (which you can do with any plain text editor), you will see that it looks nothing like a typeset page yet, it might even look worse than the BPT-file you started with. This is because `.tex` files only contain the instruction for the typesetting, which needs to be done by a 'TeX-engine'. For the conversion of the BPT-files to PDFs we will use the [Luatex engine](https://luatex.org) because it supports foreign scripts, RTL-text, and other specific options which are needed for critical text editions.

8. Run latexmk. Latexmk is included with most LaTeX installations. It enables us to process the files until they are done. Our `.tex` files would need a couple of runs of the standalone lualatex-engine otherwise. (This is due to our parallel typesetting.)
```
$ latexmk --lualatex path/to/folder/containing/files
```

You will need to have the Brill font installed. Please follow [this link](https://brill.com/page/1228?language=en "Brill font") for more info on how to install it.)

9. \[Optional\] If latexmk gives an error, or you want to work on the TeX-file itself, e.g. to debug or to try a different styling. Use a TeX-editor like TeXshop, included with MacTeX, to open and to typeset the file. __You will need to run the typesetting command at least 3 times in order to typeset line numbers and footnotes correctly.__

10. Check for the [issues labeled 'manual step for PDF generation in the bpt-converter repository](https://gitlab.com/brillpublishers/code/bpt-converter/-/issues/?label_name%5B%5D=manual%20step%20for%20PDF%20generation) for manual steps to optimize the conversion.

11. Check and review the conversion!

N.B. Development has started on a small web interface for the BPT-converter which could be hosted on Brill's servers: <https://gitlab.com/brillpublishers/code/bpt-preview-server>. This will output PDFs directly. It is not in production at the moment.

### From now on:
1. Change into the repository folder.
2. Activate the virtual environment
3. Run the BPT-converter.
4. Run `latexmk` with the required arguments.

N.B. On a repository update, you will need to pull the repository (run `git fetch` and `git pull`) and reinstall the BPT-converter again into the virtual environment (Step 4).

### Conversion options
There are several options available to you if you want to convert to PDF. These options can be enabled using 'flags' in the command line. Not calling an option applies its default value.

```bash
bptconverter --flag-option=option-to-select
```

- `-u` or `--use`: choose from the following options: `internal`, `author`, or `print`, defaults to `print`. This is used for applying styles conditionally according to the audience targeted. Only changes the footer text for now.
- `-foot` or `--footer-text`: input text to be typeset as footertext. Overrules the default set by `--use`.
- `-cit` or `--citation-style`: set the citation style to be used in the document. Default is `chicago-author-date`. A list of available styles can be found [here](https://github.com/citation-style-language/styles/).
- `-pn` or `--page-number-start`: set the page number of the 'title page' of the entry. Used if one wants to set the entry as a chapter of a larger work.

## Background
In the spring of 2021 work was started by [André van Delft](https://gitlab.com/andredelft) on a new output format for the [BPT-converter][1]. I, [Johannes de Wit](https://gitlab.com/johannesdewit), have also been contributing to the project. The effort lives on the [print branch of the BPT-converter](https://gitlab.com/brillpublishers/code/bpt-converter/-/tree/print) and is still very much in development.

The project, initiated by Ernest Suyver, seeks to find a solution to what has been called the 'BPT to print problem'. The idea had been to use one standard as the backbone for all datafied texts used in the Scholarly Editions environment. [BPT, Brill Plain Text](https://brillpublishers.gitlab.io/documentation-brill-plain-text/), a flavor of [Markdown](https://daringfireball.net/projects/markdown/) was chosen and formulated. The [BPT-converter][1] was built in order to convert these BPT-files into CTS XML which is used by the online reader. Since all information for rendering the texts according to the requirements would be there in the BPT-file, attempts were made to render typeset, print-ready PDFs from the BPT-file. [The Pandoc route](../print-pandoc-route/print-pandoc-route.md) was one such attempt (which was not succesful), the BPT-converter route is another.

Since the BPT-converter allready had a structure in place to convert BPT-files, the idea was coined to create a different output rendering which would create `.tex` files. These could in turn be converted into PDFs. The route via `.tex` seems to be the only viable option I have encountered which enables the conversion of great quantities of files with minimal manual work. For more information on the specifics of the TeX to PDF conversion, see below.

Creating PDFs from the BPT-files is desirable for two reasons:
1. To create a PDF which could be send to a typesetter as basis. This would cut costs compared to sending just the BPT to the typesetter.
2. To be able to easily convert BPT-files to PDFs to preview the work an author or editor has done and to be able to comment on it.

## Reviewing problems in the output
Because of the generation process mistakes in the PDF that was generated can have three sources:
1. The BPT source file.
2. The BPT to TeX conversion.
3. The TeX to PDF conversion.

Problems in the last conversion should only arise if faulty TeX was put out by the conversion to TeX (step 2). This does not mean that the converter itself is always to blame. Sometimes the TeX can be faulty because of a problem with the source file. Always check the source, the BPT-file, if a file has not converted as expected.

Please file an issue on Gitlab if the source file is correct but the conversion is faulty or could not complete. Add any information you have concerning the problem.

### Citeproc errors
When something goes wrong with the processing of the citations the converter will throw a 'HTTP 500 Citeproc error'. This means the citeproc server was reached, but it could not process the request properly.One common source of problems with converting is mistakes in the bibliography. __This will need to be remedied in the BPT-file itself.__

I have remedied this in the past by narrowing down the error to a certain entry in the bibliography. Missing values for fields like `publisher` and `publisher-place` are common causes for the problem. Also specifying fields that are not allowed for the bibliography entry type is a cause of errors.

### Faulty text or text formatting
If the text displayed is not correct or not formatted correctly, please check the BPT-file first before moving on to the converter or the TeX file. We want the source files to be near perfectly formatted. 

- TeX syntax showing probably indicates a missing or an extra TeX tag `{` or `}`. This is most likely caused by a problem with the converter.
- Markdown syntax showing could indicate a missing or an extra TeX tag, faulty Markdown, or problems with the converter.

### If problems persist
If you are not able to solve the problem in the conversion yourself, and there is no [issue](https://gitlab.com/brillpublishers/code/bpt-converter/-/issues) which already indicates the problem, please [file an issue and in the bpt-converter repository](https://gitlab.com/brillpublishers/code/bpt-converter/-/issues/new) detailing your problem elaborately (e.g. with clear screenshots) and assign it to me (@johannesdewit). I am doing some support work on the bpt-converter print branch (approximately 2h per week only!).

I will get a notification if you mention or assign me, but if you do not, I will probably miss the issue. So please assign me!

I will read your issue and see if I can think of a way to fix it. If I can, I will make a branch referencing your issue, which you can checkout and see whether it has fixed your problem. If I cannot fix it in the little time I have available to me, I will write a way to work around it and the issue will remain on the repository labled [_manual step for PDF generation_](https://gitlab.com/brillpublishers/code/bpt-converter/-/issues/?label_name%5B%5D=manual%20step%20for%20PDF%20generation). I will not be working on these issue in the coming months.

If you need me to do a conversion for you or need help on something else please reach me via [e-mail](mailto:johannesdewit@outlook.com). Please keep in mind the fact that I only have little time to spend on this project as of now.

[1]: https://gitlab.com/brillpublishers/code/bpt-converter
