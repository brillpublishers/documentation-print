---
author: Johannes de Wit
status: published
completed: 18/04/2022
---

# Pandoc-route report
## Introduction
From the summer of 2021 until April 2022 there have been attempts by
[Johannes de Wit](https://gitlab.com/johannesdewit) to provide a
way to convert [BNJO][1] [Brill Plain Text files (BPT-files)][2] to print-like
pdf's adhering to the [BTS][3] using [Pandoc][4]. These attempts have __not__
proved fruitful. This document aims to summarise the process, indicate the
challenges that we faced and provide a background for anyone looking into
continuing this project or setting up a project like it.

Note: There have been earlier attempts by Brill to achieve something similar.
See [`1st-attempt-2018/Notes.md`][5]. I had no knowledge of this earlier attempt
when starting developing this project. I have repeated several of the steps also
taken by the author of this earlier attempt.

[1]: https://gitlab.com/brillpublishers/data/jo
[2]: https://brillpublishers.gitlab.io/documentation-brill-plain-text/
[3]: https://gitlab.com/brillpublishers/documentation-print/-/blob/0b37e82cec68ca78667e0db31a104b23545612da/documents/BTS/BTS_2-0_print.pdf
[4]: https://pandoc.org
[5]: ../1st-attempt-2018/Notes.md

## The goals
The goals of the project were:
1. Providing a way for authors and copy-editors to review and comment on their
work in a semi-typeset manner. The formatting helps judging and communicating
possible errors. Markdown itself is not really fit for this purpose.
2. Providing these options from within a markdown editor like [Obsidian][6] or
[Typora][7], which both have the possiblity of engaging Pandoc as a [plug-in][6a] or a
built-in feature respectively.
3. By using Pandoc we would bypass the [BPT-converter][8]. This Brill-built
application has proved to be difficult to work with for those who are not
comfortable with the command line. The automation of the process with GitLab's
CI/CD on the [JO repository][1] is both heavy on processing power and slow.
4. As a bonus, a conversion route via Pandoc may give us options to convert BPT
to other file formats as Pandoc is built to do just that.

[6]: https://obsidian.md
[6a]: https://github.com/OliverBalfour/obsidian-pandoc
[7]: https://typora.io
[8]: https://gitlab.com/brillpublishers/code/bpt-converter

## Pandoc
Pandoc is an open source command line application. It is very powerful for
converting a wide array of text formats into other file formats. It is very much
focused on converting markdown files to other formats as it seems to be
developed with the idea to ease the writing process in markdown without giving
up compatibility.

### Use
Pandoc, when installed, can be used as a command line tool like so:
`$ pandoc -o output.file input.file`. Additionally one can automate this
in an application or plug-in, greatly improving ease of use for those who are
not to comfortable using the command line. (Which is what Typora and Obsidian
provide.) Several flags can be specified to allow for specifying more options,
like `--template TEMPLATE.FILE` and `--filter=FILTER.FILE` which allow for
greater modification of the processing.

### The conversion
A Pandoc conversion consists of three stages. First, a certain file is 'read' by
pandoc using a file type-specific [_reader_][9]. This converts the file into
what is called the Abstract Syntax Tree (AST). This is in turn converted to the
wanted file type by a [_writer_][9] according to a [_template_][10]. The AST
will be the same for any file format converted. This is pandoc's own way of
grasping the syntax of the source file. It basically contains every element of
the file and specifies the role of that specific element.

> Input file → AST → Output file

The reader, writer, and template can all be modified individually. Adding to
those three there is the [_filter_][11] option which modifies the AST itself.
This filter should be written in the Lua scripting language. The filter and the
template can be modified the easiest. Readers and writers are only modified when
a new file extension should be supported and are not written very accessibly. In
most occasions writing custom filters and templates should give you all the
options desired.

> Input file → |_reader_| → AST → |_lua filter_| → AST → |_writer_ & _template_|
  → Output file

[9]: https://pandoc.org/MANUAL.html#custom-readers-and-writers
[10]: https://pandoc.org/MANUAL.html#templates
[11]: https://pandoc.org/filters.html

### Filter or template?
The Lua filters are used mainly to change the order, the text formatting, and to
insert variables if placeholders are used. Filters mainly deal with the
semantic content of the file. Templates contain more information concerning the
typesetting, the formatting of the end result. This should thus be modified if
one wants to change the look of the output file. This distinction is however not
very strict. There can be specified semantic changes in the templates as well.

### Converting to PDF
When converting to a PDF one extra step should be added. Pandoc is not capable
of generating PDF-files in and of itself. It requires a TeX-engine to do the
final step instead. Pandoc writes a `.tex` file and engages the tex-engine to
generate the pdf. There are several TeX-engines which can be used. For our
purpose [Lualatex][12] is the best choice. It supports custom
fonts (like the Brill font) and can handle right-to-left text.

> Input file → AST → |_template_| .tex File → |_LuaLaTeX-engine_| → PDF

We can also apply certain settings to this last step of the conversion. There
are two ways we can do this: by providing certain specification in the tex-file
by modifying the Pandoc template, or by specifying them ourselves as options in
the `.tex` to `.pdf` conversion.

[12]: https://luatex.org

## Input and desired output
### Input: Jacoby as BPT file
Brill's New Jacoby (BNJO) works are written in the [BPT format][2], which
contains several parts:

1. Work metadata (in [yaml][13])
2. Fragments
  1. Fragment metadata (in [yaml][13])
  2. Fragment text
  3. Fragment translations
  4. Fragment commentary
3. Work commentary
4. Bibliography

It also contains two types of notes: footnotes, made by the author in both the
work commentaries and the fragment commentaries, and _apparatus criticus_ notes,
which adhere to the text editions. The footnotes in the BPT files do not adhere
to the more common markdown formatting of `[^#]: ` but leave out the column,
like so: `[^#] `.

Note: Every Fragment contains metadata, text, translation and a commentary, in
that order. A work (one BPT-file) can contain multiple fragments.

[13]: https://yaml.org/

### Output: A typeset PDF
The desired output is a typeset PDF which adheres as close as possible to the
[BTS][3]. While it is obvious not possible to both automate the process and
produce an output which is print-ready, it should be presentable and give a good
overview of the information which was put in the BPT file.

The order of the individual parts that make up the PDF depends on the product
that is being worked on. The default BTS format differs from the order used in
'real world' print editions of the Jacoby series up till now.

The biggest thing is the Jacoby-specific demand of printing the fragments and
their translations side-by-side on opposing pages.

## Process
In order to produce the PDF a couple of things are needed:
- Metadata needs to be processed:
  - Metadata fields like 'author' and 'title' can be used as metadata for
  the PDF as well.
  - All fragment metadata and much of the work metadata is only relevant for
  the online reader environment and not for the print edition, and thus needs to
  be removable.
- Order of the file should be modifiable according to either the existing order
of the print-editions or [BTS order as specified here][16].
- Footnotes should be processed:
  - Seperation between the 'regular' and AC footnotes needs to be made.
  - Footnotes should be picked up by Pandoc, despite using a different
  formatting.
- BTS-compliant styling and typesetting needs to be applied to the end product.
- Fragments and their translations need to be printed side-by-side.

[16]: https://gitlab.com/brillpublishers/documentation-print/-/blob/0b37e82cec68ca78667e0db31a104b23545612da/documents/create_print_JO/typography-bnjo.pdf

### Modifying Filters
#### Metadata processing
Using the existing work metadata to generate the metadata for the PDF is done
automatically by Pandoc as this conforms to [Pandoc's own markdown][14]
specifications.

Since filters allow for changing the content of the processed file effectively
we can remove the fragment metadata by writing a filter. This was done
successfully and the filter can be found [here][15].

#### Changing BPT order
Pandoc's filters were also used to change the order to match the desired order.
Since the BPT-files are not ordered to match either one of the specifications
a filter must always be used when converting via Pandoc. Two filters have been
written: [One matching the print order][17] and
[one matching the BTS specifications][18].

[14]: https://pandoc.org/MANUAL.html#pandocs-markdown
[15]: https://gitlab.com/johannesdewit/bnjo-pandoc-luafilters/-/blob/c09a6dbcf2f6f43836d11ef989028e66c1c7a221/remove_codeblocks_yaml_only.lua
[17]: https://gitlab.com/johannesdewit/bnjo-pandoc-luafilters/-/blob/c09a6dbcf2f6f43836d11ef989028e66c1c7a221/restructure_to_BNJOiv.lua
[18]: https://gitlab.com/johannesdewit/bnjo-pandoc-luafilters/-/blob/c09a6dbcf2f6f43836d11ef989028e66c1c7a221/restructure_to_BTS.lua

### Modifying reader
During the project it was found that footnotes posed a challenge for the
conversion for two reasons. As noted [above](#input:-jacoby-as-bpt-file):
1. Footnotes in the BPT do _not_ match Pandoc's markdown specifications. BPT
uses `[^#] ` to define the notes while Pandoc (and most other markdown formats)
use `[^#]: ` to define notes.
2. BPT-files contain two seperate series of footnotes: regular footnotes and
_apparatus criticus_ notes. Normally Pandoc just processes every note as
belonging to one series and renumbering them from the top. The distinction is
thus not kept.

In order to overcome these challenges a seperate [reader][9] should be built
which can deal with the BPT's footnote definition and with two series of notes.
Pandoc's standard markdown reader could be modified to support this, I expect.
This has __not__ been tried, but problems are to be expected as Pandoc does not
support several footnote series within the AST nor for the writers. Supporting
this would thus come with a large load of work at our side.

### Modifying Templates
#### BTS compliant styling
[BTS][3] compliant styling can be added just as well to the Pandoc output as
with the [BPT converter][8] because we can edit the [default template][10] to
match the BTS specifiations. Pandoc has processing done in the templates as well
which depends on variables specified in the conversion. (See
[Pandoc's docs][10].) When doing this, the custom packages and specifications
should be put in _before_ the `\begin{document}` command and after the last
specified `$if$` function. In the case of the template I started building this
can be seen between lines 445 and 569 of [the BTS template][19]. Here normal
latex packages and commands can be used that influence the typesetting of the
PDF. Flexibility of the automated typesetting is limited by the possibilities of
latex itself.

A limitation may be the use of the single variable [`$body%`][20] to generate
the file. This starts the printing of the content contained in the AST at this
place in the `.tex` file. This means no latex can be automatically put in by the
template in between e.g. headings and paragraphs. Styling can thus be limited by
this. Because the custom settings and packages we loaded are applied like a
stylesheet to a body of content, the content should contain flags or
specifications which can be addressed and style according to their function. The
BTS differentiates between the styling of Greek fragments and commentary text
for example. In markdown these are both just 'paragraphs', which is mirrored in
Pandoc's AST. If we wanted to apply different styling to either one, we should
1) define custom latex environments, 2) edit Pandoc's markdown reader in order
to differentiate paragraphs into different categories and 3) edit Pandoc's latex
writer to produce the environments instead of normal paragraphs. This is an
elaborate process which kind of defeats the purpose of using pandoc instead of
the already existing [BPT-converter][8] which allows for this greater
flexibility in applying latex styling to individual textparts.

As this template is only used to generate the `.tex` file which is the basis for
the PDF we can of course also process this `.tex` file after the Pandoc
conversion which provides more flexibility. This could be done by using scripts
or manually.

[19]: https://gitlab.com/johannesdewit/bnjo-pandoc-textemplates/-/blob/002fdef530608d5226503289ab5976960a2abb93/bts_template.latex
[20]: https://gitlab.com/johannesdewit/bnjo-pandoc-textemplates/-/blob/002fdef530608d5226503289ab5976960a2abb93/bts_template.latex#L625

### Fragments and translations side-by-side
One of the features of the BNJO print editions is the Greek fragments printed
side-by-side with their translations. The only viable way doing this in latex
seems to be the [_reledpar_ package][21], a strong tool which comes with some
serious limitations.

The first of these limitations is that it needs to run multiple times in order
to typeset the facing pages correctly. This is by design, because the pages need
to be rendered seperately first before they can be aligned. Pandoc does not have
a way to specify the amount of times the Luatex processor runs. This introduces
the requirement for a seperation between running Pandoc and Luatex and running
luatex multiple times which greatly reduces usability, especially for those not
comfortable with command line tools.

The biggest limitation is the fact that reledpar introduces its own latex
environments to print texts side-by-side. This means in practice that first all
the texts that need to be printed on verso need to be given within the 'left
page' environment, and then the ones to be printed recto in the 'right page'
environment. This does not mirror the BPT-order at all and it adds the
requirement to define en environment around all fragments and around all
translations. Which introduces all of the problems specified
[above](#bts-compliant-styling). This causes big problems because Pandoc's AST
is not a semantical hierarchy, but only portrays the file as being read top-down.
An enveloping environment can thus not be initiated.

This would mean that a custom reader would need to be written to introduce
custom 'classes' of paragraphs. These custom classes should then be processed by
a custom writer which would convert these to custom latex environments, which
by the use of e.g. a custom python script should be reordered to match the
format required by reledpar.

[21]: https://www.ctan.org/pkg/reledpar

## Conclusion
It seems to me that Pandoc at the moment is not the right choice for attaining
all of the [goals](#the-goals) we wanted to achieve, even if we would invest the
time needed to develop this path further. It would provide a way to convert to
PDF and thus provide a workflow for the authors and copy-editors to review and
comment on their work. However, at the moment of writing, this cannot be done
from within the markdown editor itself because Pandoc needs to be called with
very specific options and after running Pandoc it is required to run Luatex at
least three times to generate a representative PDF. Both operations require the
use of the command line and a basic knowledge of both Pandoc and Luatex or other
LaTeX-engines. Pandoc thus does not provide an accessible way to convert to a
semi-typeset PDF for the BNJO BPT-files as of now. The possibility of also
converting to other file types using this method is greatly problematized
because of all the customizing (e.g. writing a custom writer) that would be
required to enable this functionality.

Pandoc could be reconsidered for converting BPT-files and using it in internal
communications and validation if we let go of the idea that it should look like
a semi-typeset PDF. When using for example the Obsidian plug-in it is quite
usable for the average user. The standard output of Pandoc when converting
BPT-files is not up to it now, but using the filters and applying a little
styling this could work. We should however drop the idea of typesetting
side-by-side. Pandoc is however also not up-to-it when working with Leiden+ and
the apparatus criticus.

Concluding, Pandoc does not offer greater usability than the BPT-converter would
at the moment. Introducing Pandoc into the workflow of authors and copy-editors
is not a viable option at the moment, and the work that is needed in order to
improve both its results and usability is huge.



NB: For an impression of the results compare [default Pandoc output of a BPT-file](documents/1783_default_pandoc.pdf) and [the output with a template](documents/1783_template.pdf).

## Resources
### Repositories belonging to this project

Also available in the `documents` folder.

- [BNJO Pandoc luafilters](https://gitlab.com/johannesdewit/bnjo-pandoc-luafilters) (Archived)
- [BNJO Pandoc TexTemplates](https://gitlab.com/johannesdewit/bnjo-pandoc-textemplates) (Archived)

### Repositories relating to this project
- [Jacoby (JO) data repository][1]
- [BPT-converter][8]
