# Print: Pandoc route

Information and files belonging to two attempts to convert [Brill Plain Text](https://brillpublishers.gitlab.io/documentation-brill-plain-text/)
to PDF via Pandoc and LaTeX.

1. [First attempt](1st-attempt-2018/Notes.md) was made in 2018.
2. [Second attempt](2nd-attempt-2021/report.md) was made in 2021.

Both attempts have produced template files for Pandoc to mirror a print
edition as close as possible. Neither were succesfull. However, reading both
the notes on the project will give the reader a pretty good idea of working
with BPT-files, Pandoc and LaTeX in typesetting and text-editions
applications.
