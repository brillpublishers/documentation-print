Notes on Brill-template.tex
===========================

1 Introduction
==============

The TeX-template is created in order to convert (using Pandoc) a
plaintext file (e.g., a Markdown-file) into a PDF-file
that conforms to the Brill Typographic Style, version 2.0 (March, 2018).

1.1 What is TeX?
----------------

This process makes use of TeX, a mark-up language that has its goal in
producing high-quality typesetting. It is used in different fields of
academia, as it is an excellent way to deal with, e.g., mathematical
notations and foreign languages. In order to be able to use TeX for
conversion to PDF, one needs software in the form of a TeX
driver (e.g., MiKTeX) to go from .tex to .pdf. Additionally, a TeX
editor (e.g., TeXworks) facilitates the editing of a .tex file.

1.2 Incorporation in Pandoc
---------------------------

The Brill-template.tex should be included as a template in the Pandoc
command line. Some other commands are needed for proper use of this
template (see below), giving the following minimal Pandoc command:

```
Pandoc <INPUT> --template=Brill-template.tex --pdf-engine=lualatex --toc --listings --top-level-division=chapter -o <OUTPUT>
```

The output can be either a .pdf file or a .tex file. Furthermore, there
are several variables that can be included to adjust the output to the
specific needs of the document. These are presented in section 4 and can
be inserted in the Pandoc command line preceded by `--variable=`; e.g.,
`--variable=grid`.

2 TeX specifics
===============

Especially because of its support of OpenType fonts and unicode
character typing (which are not supported by the regular pdfLaTeX
extension), I have chosen to use LuaLaTeX extension for this template.
The advantages of the LuaTeX engine over the XeTeX engine are minor (if
existing at all), but I have no experience with the latter.

Furthermore, the template only covers the type of document TeX describes
as `\documentclass[book]`. By default, this format uses `\chapter` as
its highest section level[^1], but this has to be specified in the
Pandoc command line (`--top-level-division=chapter`; see above). In order
to function well for other document types, the template needs to be
adapted.

3 Brill Typographic Style features applied
==========================================

The following features appearing in the Brill Typographic Style have
been included in the template.

3.1 In the Preamble
-------------------

In TeX documents, the preamble is everything before the command
`\begin{document}`, which starts the contents of the final document.
These contents are formatted according to the commands and parameters
set in the preamble (although it is possible to alter some things within
the document's contents). Packages can only be loaded in the preamble.

### 3.1.1 Page layout and fonts

| **feature**     | **BTS page**    | **TeX solution** | **notes**       |
| --------------- | --------------- | --------------- | --------------- |
| no blank pages <br/> between chapters | p\. 14          | `\documentclass[...,openany]` | Changes the default behaviour of <br/>TeX to start a new chapter always <br/>on a right-hand page. Eliminates <br/>blank pages between chapters. |
| page size       | p. ix, xiv | `\usepackage{geometry}` <br/> `\geometry{...}` | The `geometry` package allows <br/>extensive adjustment of the page <br/>layout, including page and margin <br/>sizes. Note that for some reason <br/>`geometry` does not work with <br/>LuaLaTeX unless one of the <br/>packages `graphicx`, `color`, or <br/>`xcolor` is used. This is the case <br/>in this template. |
| baseline grid   <br/> (horizontal) | p\. 1                 | `\usepackage{grid}`<br/> `[baseline=4.75mm,\lines=40]` | The horizontal grid (distance <br/>between the lines: 4.75 mm) is a <br/>feature that is uncommon for TeX-<br/>documents. The `grid` package was <br/>written for such cases, but it <br/>has several concomitant problems <br/>(see section 5). It appears to <br/>impossible to incorporate the use <br/>of a reserve line. |
| baseline grid   <br/> (horizontal) | p\. 1                 | `\newcommand{gridbox}[1]{\raisebox{`<br/>`0mm}[4.75mm][0mm]{#1}}` | In aid of the baseline grid, I <br/>have created a {gridbox} command <br/>that treats the text in it as if <br/>it were exactly 4.75 mm in height. <br/>This is applied as often as <br/>possible (viz., in chapter and <br/>section titles, and on the pages <br/>of the preliminary matter). |
| baseline grid   <br/> (vertical)                 | p\. 1 | various, e.g., <br/> `\setlength{\parindent}{4mm}` | The vertical grid is applied <br/>throughout the template, in those <br/>cases where it is relevant, e.g., <br/>in the paragraph indentation. |
| Brill font | p\. 1 | `\documentclass` <br/> `[11pt,...]` <br/> `\usepackage{fontspec}` <br/> `\setmainfont{Brill}[Ligatures=TeX]` | The font size of the body text is <br/>set in the `\documentclass` <br/>command, while the LuaLaTeX <br/>extension in combination with the <br/>`fontspec` package makes it <br/>possible to change the main font <br/>to Brill. Additional font features <br/>can be added between the square <br/>brackets of the `\setmainfont` <br/>command. |
| fonts for <br/> coding and      <br/> other scripts | -- | `\setmonofont{Consolas}[Scale=Match`<br/>`    Lowercase]`<br/>`\newfontfamily{\HebrFont}{SBL He`<br/>`    brew}[Scale=MatchUppercase]`<br/>`\newcommand{\HebrText}[1]{\bgrou`<br/>`p\textdir TRT\HebrFont{\#1}\egro`<br/>`up}`<br/>...and following lines | The template also sets a monospace <br/>font (Consolas), a Hebrew script <br/>font (SBL Hebrew), a Hebrew script <br/>monospace font (Everson Mono; not <br/>recommended but no alternative <br/>found), and an Arabic script font <br/>(Scheherazade). The additional new <br/>commands take care of the <br/>right-to-left text direction that <br/>is needed with these scripts[^3] . |
| kerning, letter <br/> spacing, and    <br/> glyph scaling | p\. 1, 87, 91 | `\usepackage[activate={false,compat`<br/>`ibility},tracking=true,stretch=0,s`<br/>`hrink=0]{microtype}`<br/>`\SetTracking{encoding={\*},shape=s`<br/>`c}{50}` | In line with the BTS, all kerning, <br/>letter spacing, glyph scaling, and <br/>similar features are fixed to , <br/>zero to remain undistorted. The <br/>only exception concerns small <br/>caps, to which a small amount of <br/>tracking (+50/1000) is applied. |

### 3.1.2 Figures and Lists

| **feature**     | **BTS page**    | **TeX solution** | **notes**       |
| --------------- | --------------- | --------------- | --------------- |
| figure rescaling | p. 38          |  `\usepackage{graphicx}`<br/>`\makeatletter`<br/>`\def\maxwidth{\ifdim\Gin\@nat\@wid`<br/>`th\>\textwidth\textwidth\else\Gin\`<br/>`@nat\@width\fi}`<br/>`\makeatother`<br/>`\let\Oldincludegraphics\includegra`<br/>```phics{\catcode\`\@=11\relax\gdef\i```<br/>`ncludegraphics{\\@ifnextchar[{\Old`<br/>`includegraphics}{\Oldincludegraphi`<br/>`cs[width=\maxwidth]}}}` | The use of the `graphicx` package is <br/>the default way to deal with <br/>figures for the Pandoc template as <br/>well as TeX in general. However, <br/>the code provided in the Pandoc <br/>template, which rescales images to <br/>fit within the width of the text <br/>(as is required by the BTS as <br/>well) returns an error that is <br/>solved by the current code. |
| default figure  <br/> placement   | (p. 38) | `\makeatletter\def\fps\@figure{!t}`<br/>`\makeatother` | Puts figures on top of the page. <br/>This is not necessarily prescribed <br/>by the BTS, but it seems to be <br/>preferred. |
| zero space      <br/> between list    <br/> items | p. 5-14 | `\providecommand{\tightlist}{\setle`<br/>`ngth{\itemsep}{0pt}\setlength{\par`<br/>`skip}{0pt}}` | This is a command from the Pandoc <br/>default template, and so it <br/>appears in the TeX code and cannot <br/>be deleted. However, this is not a <br/>problem, as it concords with the <br/>BTS. |
| EN dash as list <br/> identifier  | p. 5-14 | ```\def\labelitemi{\--}``` | Sets item identifier to EN dash <br/>(–). |
| list            <br/> indentation | p. 5-14 | `\usepackage{enumitem}`<br/>`\setlist[enumerate]{...}`<br/>`\setlist[itemize]{...}` | The `enumitem` package allows for <br/>the alteration of lists (TeX: <br/>itemize) and enumerations (TeX: <br/>enumerate), including its <br/>indentation. |
| enumeration     <br/> identifiers | (p. 5-14) | `\setlist[enumerate]{...}`<br/>`\setlist[enumerate,2]{...}`<br/>`\setlist[enumerate,3]{...}`<br/>`\setlist[enumerate,4]{...}` | There appears to be no Brill <br/>standard for the identifiers <br/>throughout the various levels of <br/>an enumeration. The code here sets <br/>it as follows: 1. → a. → i. → 1. <br/>(without addition of the parent <br/>label). These commands rely on the <br/>`enumitem` package. |

### 3.1.3 Headlines, page numbering, and headings

| **feature**     | **BTS page**    | **TeX solution** | **notes**       |
| --------------- | --------------- | --------------- | --------------- |
| layout of       <br/> running         <br/> headline | p. 47 | `\usepackage{fancyhdr}`<br/>`\fancypagestyle{main}{...}`<br/>`\fancypagestyle{front}{...}` | The running headline can be <br/>adjusted in many ways by using the <br/>`fancyhdr` package. The template <br/>creates two distinct styles, one <br/>applied to the front matter (i.e., <br/>everything before the body text), <br/>and one applied to the main <br/>matter. Both are adjusted to the <br/>BTS The header also includes page <br/>numbers. |
| front matter    <br/> titles in small <br/> caps in running <br/> headline   | p. 47 | `\usepackage{etoolbox}`<br/>`\patchcmd{\tableofcontents}{\MakeU`<br/>`ppercase}{}{}{}`<br/>`\patchcmd{\listoffigures}{\MakeUpp`<br/>`ercase}{}{}{}`<br/>`\patchcmd{\listoftables}{\MakeUppe`<br/>`rcase}{}{}{}` | For regular chapter titles, the <br/>adjustment in small caps can be <br/>made in the `\fancypagestyle` <br/>configuration. The titles of the <br/>front matter are fixed values in <br/>TeX, and can only be altered by <br/>the use of a package like `etoolbox` <br/>and its `\patchcmd` command. |
| no page numbers <br/> on chapter      <br/> title page or   <br/> empty page | p. 21, 47                 | `\makeatletter`<br/>`\let\ps\@plain\ps\@empty`<br/>`\def\cleardoublepage{...}`<br/>`\makeatother` | This code removes the page <br/>numbering from chapter title pages <br/>and empty pages by applying a page <br/>style `{empty}` (by default <br/>available with the `fancyhdr` <br/>package). At the same time, it <br/>also preserves the distinct <br/>even/odd format when empty pages <br/>between chapters are removed. |
| chapter title   <br/> page layout                 | p. 20 | `\makeatletter`<br/>`\def\@makechapterhead\#1{...}`<br/>`\def\@makeschapterhead\#1{...}`<br/>`\makeatother` | Adjusts chapter title page <br/>properties to BTS. TeX has a <br/>difference between a regular <br/>`\chapter` (with chapter number) and <br/>a 'starred' `\*chapter` (without <br/>chapter number). Both are treated <br/>by this code. |
| no chapter      <br/> numbers with    <br/> section numbers | - | `\makeatletter`<br/>`\renewcommand{\thesection}{\@arabi`<br/>`c\c\@section}`<br/>`\makeatother` | By default, TeX adds the chapter <br/>number in the section number for <br/>all underlying levels. This code <br/>prevents it from doing so. |
| heading format  | p. 21-27       | `\usepackage{titlesec}`<br/>`\titleformat{\section}...`<br/>`\titlespacing*{\section}...`<br> and following lines. | The `titlesec` package allows to <br/>adjust both the format and the <br/>spacing of section headings. The <br/>template has done this up to the <br/>level of `\subparagraph` (the 5^th^ <br/>level after `\chapter`); further <br/>levels are not available by <br/>default in TeX (but can be added <br/>by using packages). |

### 3.1.4 Preliminary matter

| **feature**     | **BTS page**    | **TeX solution** | **notes**       |
| --------------- | --------------- | --------------- | --------------- |
| format of Table <br/> of Contents | p. 59-60        | `\usepackage[titles]{tocloft}`<br/>`\cftsetindents{chapter}{0mm}{4mm}`<br/>...and following lines.<br/>`\renewcommand*{\cftpnumalign}{l}`<br/>`\renewcommand*{\cftchapleader}{\hs`<br/>`pace{2ex}}`<br/>`\renewcommand*{\cftchapafterpnum}`<br/>`{\cftparfillskip}`<br/>`\renewcommand*{\cftchappagefont}{}` <br/>...and following lines. | With the `tocloft` package, the <br/>Table of Contents (ToC) can be <br/>adjusted precisely to the standard <br/>of BTS. For each section level, <br/>the indentation, space between <br/>chapter title and page number, and <br/>font formatting are all separately <br/>described in the template. The <br/>title of the Table of Contents is <br/>by default the desired *Contents*. |
| half-title page <br/> at beginning | p. 51          | `\makeatletter`<br/>`\newcommand*{`<br/>`\cleartorightpage}{...}`<br/>`\makeatother`  | Because Brill's books start with a <br/>left-hand page (viz. the <br/>Half-title page), we need a new <br/>command to force a page to begin <br/>at a right-hand page (default is <br/>left-hand). |

### 3.1.5 Footnotes and Quotations

| **feature**     | **BTS page**    | **TeX solution** | **notes**       |
| --------------- | --------------- | --------------- | --------------- |
| footnote <br/> indentation        | p. 3-5    | `\usepackage[hang]{footmisc}`<br/>`\renewcommand{\hangfootparskip}{0m`<br/>`m}`<br/>`\renewcommand{\hangfootparindent}{`<br/>`4mm}`<br/>`\makeatletter`<br/>`\patchcmd{`<br/>`\@makefntext}{...}{...}{}{}`<br/>`\makeatother` | For the indentation of footnotes <br/>we make use of the package <br/>`footmisc` for regular behaviour, <br/>and of some extensive additional <br/>code to account for the <br/>fluctuating left margin that <br/>depends on the number of digits of <br/>the footnote number (needs <br/>`etoolbox` package already loaded <br/>for getting front matter titles in <br/>small caps). |
| block <br/> quotations            | p. 5 | `\usepackage{quoting}`<br/>`\let\quote\quoting`<br/>`\quotingsetup{...}`   | Pandoc puts all block quotations <br/>in the `{quote}` environment, but <br/>this is not formattable to our <br/>needs. Instead we use the `quoting` <br/>package and adjust the spaces <br/>around the quotation with <br/>`\quotingsetup`. |

### 3.1.6 Hyperlinks and Code Environments

The incorporation of commands to deal with source code (for TeX usually
called 'verbatim') was especially relevant for the testing document, the
TEI Guidelines, which had a large amount of code blocks.
The more adjustable listings package was preferred above fancyvrb,
primarily because it allowed to extend to range of characters to be
handled in code blocks to include, e.g., Greek (based on
UTF-8 input).

If no to be printed source code occurs in the publication, this can be
ignored and the listings variable in the Pandoc command line can be
omitted.


| **feature**     | **BTS page**    | **TeX solution** | **notes**       |
| --------------- | --------------- | --------------- | --------------- |
| regular font    <br/> for URLs    | - | \urlstyle{same}   | Uses the contextual font for URLs <br/>instead of the monospace font. |
| line breaks in  <br/> URLs    | - | `\def\UrlBreaks{\do\/\do-\do.\do=`<br/>`\do_\do?\do\&\do\%}`   | Instead of extending across <br/>margins, an URL will break at one <br/>of the given characters. |
| code blocks     <br/> format  | - | `\usepackage{listings}`<br/>`\newcommand{\passthrough}[1]{#1}`<br/>`\lstset{...}` | A fancy layout for source code <br/>blocks. Unsupported by BTS, which <br/>has no guidelines on this matter. |
| UTF-8 support   <br/> in code blocks  | - | `\makeatletter`<br/>`\lst@InputCatcodes\def\lst@DefEC{`<br/>`\lst@CCECUse\lst@ProcessLetter`<br/>`...}`<br/>`\lst\@RestoreCatcodes`<br/>`\makeatother`    | Allows for the manual listing of <br/>all UTF-8 above Unicode 0--255 <br/>characters that should be allowed <br/>in source code blocks. Unicode <br/>128--255 can be made available by <br/>including `extendchars=true`, in <br/>`\lstset`. |
| code blocks in  <br/> footnotes | -   | `\usepackage{fancyvrb}`<br/>`...`<br/>`\VerbatimFootnotes`    | Source code and footnotes do not <br/>function together well, but they <br/>do with this command (for which <br/>the `fancyvrb` package needs to be <br/>loaded). For some reason the <br/>points at which time the package <br/>and the command are loaded are <br/>said to be rather fixed. |

3.2 In the Content
------------------

The template does not only set up the parameters that shape up the
document's text, it also provides the first pages of the document that
come before the actual body (inserted by Pandoc by the command
\$body\$).

| **feature**     | **BTS page**    | **TeX solution** | **notes**       |
| --------------- | --------------- | --------------- | --------------- |
| Roman page    <br/> numbering | p. 51 | `\pagenumbering{roman}`   |   |
| no running    <br/> headlines | p. 47 | `\pagestyle{empty}`<br/>`...`<br/>`\pagestyle{front}` | See above. Just before the ToC, <br/>the command \pagestyle{fro nt} <br/>loads the running headlines in the <br/>desired format (see above). <br/>Consequently, the first visible <br/>page number is on the ToC's second <br/>page. |
| Half-title page   | p. 52         | `\vspace{19.75mm}`<br/>`\begin{minipage}[b]{\textwidth}`<br/>`\makeatletter\gridbox{\@title}`<br/>`\makeatother`<br/>`\end{minipage}` | Takes title from input document. |
| Title page        | p. 56-57      | `\begin{titlepage}`<br/>`...`<br/>`\end{titlepage}`   | Takes title and author from input <br/>document. The image of Brill's <br/>logo needs to be put in the same <br/>folder as the template. The <br/>publication's location is set as <br/>Leiden | Boston.
| Colophon page     | p. 58         |                   | Dummy with the following text <br/>centred on the page: "This is a <br/>Copyright/colophon page." |
| Table of  <br/> Contents  | p. 59-60 | `\$if(toc)\$\tableofcontents`<br/>`\cleartorightpage\$endif\$` | Actually a variable, but <br/>recommended (and therefore <br/>included in the minimal Pandoc <br/>command line above). The format of <br/>the ToC is adjusted in the <br/>preamble (see above). <br/>`\cleartorightpage` causes the <br/>first item following the ToC to <br/>start on a right-hand page (see <br/>BTS, p. 14). |
| body              |               | `\setcounter{chapter}{0}`<br/>`\setcounter{page}{1}`<br/>`\pagenumbering{arabic}`<br/>`\pagestyle{main}`<br/>`...`<br/>`\$body\$` | Sets chapter-numberi ng to 0, so <br/>that the first chapter called is <br/>numbered 1. Page-numbering is <br/>reset to 1 (Arabic numbers). <br/>Running headlines are loaded as <br/>formatted above. The command <br/>`\$body\$` is a Pandoc command (as <br/>are all between `\$`‑s). |

4 Variables
===========

There are several variables that can be called in the Pandoc command
line. Some of these were already present in the default TeX-template by
Pandoc, others have been created by the author of the Brill template.
Not every Pandoc variable has survived into this template. This overview
includes all variables that are relevant to TeX and can be altered in
the Pandoc command line.

In the Pandoc command line each individual variable needs to follow
`--variable` command, e.g., `--variable grid --variable fontsize=12pt`.
Pandoc states: "If a variable is not set, Pandoc will look for the key
in the document's metadata"; this means that some variables do not need
to be called in the Pandoc command line. The variable tables, e.g., will
be activated if the document contains tables, while the variable title
takes the publication's title from the metadata. Such variables are not
included in this overview.

The ToC is in fact a Pandoc variable (although it does not need the
command `--variable`), but it has been presented above because it is
recommended to include in the document. However, it could be eliminated
by omitting `--toc` in the Pandoc command line.

| **variable**    | **possible values**   | **notes** |
| --------------- | --------------- | ------------------------------ |
| fontsize        | `10pt`, `11pt`, `12pt` | This variable adjusts the <br/>font-size of the basic text (TeX's <br/>`\normalsize`). In principle TeX <br/>allows only the stated sizes. Note <br/>that all other font sizes <br/>throughout the document are based <br/>on the value of `\normalsize` (cf. <br/>[WikiBooks](https://en.wikibooks.org/wiki/LaTeX/Fonts#Sizing_text)). If none is set, the <br/>default is 11pt as recommended in <br/>the BTS. |
| classoption     | various | Any additional properties that can <br/>be declared as option for the <br/>`\documentclass[...]{book}`. May <br/>be repeated for multiple options. |
| smalltextframe  | -       | The BTS mentions a narrower type <br/>area (107 mm width) for, e.g, a <br/>document with marginal notes <br/>(p. 1). |
| links-as-notes  | -       | Provided by Pandoc to put any URL <br/>into a separate footnote. This may <br/>be very useful for print <br/>publications. |
| header-includes | any     | Any raw content can be added to <br/>the document's header (TeX's <br/>preamble?) by this variable, but <br/>it should be in TeX code to be <br/>included in the TeX typesetting <br/>process. See [Pandoc's <br/>documentation](http://pandoc.org/MANUAL.html#metadata-blocks). |
| lang            | any [BCP 47](https://tools.ietf.org/html/bcp47) language code   | Sets the language of the document. <br/>The template makes use of Pandoc's <br/>conversion to the `polyglossia` <br/>package. The other major language <br/>package for TeX, `babel`, is not <br/>supported by LuaTeX and XeTeX. |
| grid            | -       | Places `Grid.png` (must be in the <br/>same folder as the template) in <br/>the background of every page. This <br/>image contains horizontal and <br/>vertical grid as occurring on <br/>p. xiv of the BTS. |
| tempnotes       | -       | Adds the template notes in the <br/>margin to the PDF. In this version <br/>of the template, the only note <br/>occurs with figures in the <br/>document. |
| series          | -       | Adds a (dummy) series page to the <br/>preliminary matter (see BTS <br/>pp. 54-55). |
| dedication      | -       | Adds a (dummy) dedication page to <br/>the preliminary matter (see BTS <br/>p. 59). |
| epigraph        | -       | Adds a (dummy) epigraph page to <br/>the preliminary matter (see BTS <br/>p. 59). |

5 Issues and further steps
==========================

The template in its current form serves well to pour the test file
(Brill_TEI_Guidelinesv08.md) into a format that approaches the Brill
Typesetting Style as near as is possible for now. However, there are
still many steps to be taken and problems to be solved in order to let
it function properly for any document.

5.1 Concerning the baseline grid
--------------------------------

In the first place, the typical horizontal grid of the BTS
is difficult to imitate for TeX, or at least so by the knowledge of the
template's author. The `grid` package only handles this problem to a
certain extent, but it is not able to fix all baselines to this grid.
Moreover, it causes floats like tables and images to be badly positioned
and it is not capable of aligning those and other elements to the
baseline grid.

In order to approach the baseline grid a little more, several elements
(mostly titles) have been put in a `\gridbox`, which treats them as if
they are 4.75 mm (the distance between two lines in the horizontal
grid). This, however, is impossible to do with text that extends beyond
a single row. Because this does not occur in the test file and the
problem is linked to other issues of the template, no solution has been
found.

A better, but very thorough solution, would be to discard the `{book}`
document class and design document classes from scratch. Regarding a
document class as a layout standard that formats the content, this is
the ideal place to set up the rules as specified in the
BTS, as is done regularly by publishers working with TeX.
Possibly that could also be the place where a baseline grid can be set
up. Additionally, the creation of different document classes for
different publication types would ease the process significantly.

5.2 Bibliography
----------------

Because the test file does not have any bibliographical references,
there have been no changes made to the code concerning bibliographical
matters as it appears in the default Pandoc TeX-template.

It can be said that the package `biblatex` is preferred above `natbib` as it
capable citations in humanities style rather than natural/social science
style.

5.3 Capitals vs. small caps
---------------------------

On the subject of small caps, the Brill Typesetting Style states the
following (p. 91):

> All acronyms consisting of more than two letters, with or without full
> stops, such as BCE, UNESCO, FAO, should be set in small caps, tracking
> +50/1000: [bce]{.smallcaps}, [unesco]{.smallcaps}, [fao]{.smallcaps}.

The template's author has done some research, but it appears that it
cannot be done automatically by means of a code that affects the entire
document. Such a command will probably also affect those all-caps words
listed as exceptions, or it may spoil the TeX code. Rather, a solution
should be looked by means of a list of acronyms that occurs in each
document, or an accumulating list of all acronyms occurring throughout
Brill publications. An easier approach would be, of course, the
formatting of such acronyms in the source text, either manual or
automatically with a script.

[^1]: A superior `\part` (as level -1, `\chapter` being 0) can be used as
    well.

[^2]: It is not yet clear how MarkDown, Pandoc, and TeX could work
    together to switch between languages and scripts.

[^3]: It is not yet clear how MarkDown, Pandoc, and TeX could work
    together to switch between languages and scripts.
