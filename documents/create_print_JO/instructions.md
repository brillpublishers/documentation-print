# description of  Jacoby BPT and print

## generic specifications

* book size: use the measurements as given in BTS p. xiv
* running headlines can be extracted from BPT. **Later.** See BTS p. 47
* page numbers: are _not_ in BPT. See BTS p. 47

## opening page

information from YAML entry_nr; historian; historianDate; en dan dat rare symbooltje

## entries

New page:
General Commentary: 
from BPT ### commentary: Introduction, so use that title (“Introduction” in this case)
paragraphs from BPT
footnotes from BPT: [^16], footnote text from BPT: ### Notes
BTS p.  3
New page:
Commentary per fragment
Title: Commentary
BTS p.  21
Commentaar, nootjes, gene nieuwe pagina of zo
New page:
Bibliography from BPT ### bibliography: Bibliography
Title = Bibliography
The content is within ```bibl ``` within CSL JSO in it
Moet dus geconverteerd naar bibtex
BTS p.  71
Jacoby Bibliographical Style in CSL, author names in smallcaps, family name, initials; all titles in italics, pub year achteraan
New page:
Testimonia as title, from BPT: value of textpart T1 = Testimonia
Fragment number from BPT: value of tetxpart
Source information: BPT ```yaml source: value```
Followed by : and a space
Dan de content: #### edition. Ignore title and use content
This on the left page
On the right page, same thing, maar dan met #### translation
Footnotes: see above
New page: fragmenta
Work title, from BPT ```yaml workMentioned: value```
Source information: BPT ```yaml source: value```
Followed by : and a space
Dan de content: #### edition. Ignore title and use content
Notes



## bibliography

* Brill Typographical Style, "G:\Publishing Projects\ARC\CLS\3 Major Works_MRWs\Jacoby Online\JO Subprojects\Scholarly_Editions\create_print_JO\BTS\BTS_2-0_print.pdf"
* Christina's short-lived attempt contains tex code on pp. 8-10, "G:\Publishing Projects\ARC\CLS\3 Major Works_MRWs\Jacoby Online\JO Subprojects\Scholarly_Editions\create_print_JO\Christina_attempt.pdf"
