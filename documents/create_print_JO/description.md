# Mapping .md to .teX using print layout

## Measuring: see Pim style guide p. 14
### Main paper format: 235x155
All other measurings: see Pim style guide: https://data.brill.com/media/BTS_2-0_print.pdf

Running head lines, coming from:
For now, just say "running head lines" 

page numbers:
generate

Entry opening page:
From bpt-yaml:
* entry number
* historian
* historian date
* three dots

Introduction/biographical essay (depending on content): this is commentary on entire entry
From bpt:
### commentary: introduction/biographical essay 
Divided into paragraphs

Text of footnotes: from bpt:
### Footnotes: number in text, footnote at the bottom


### Commentary on T/F (in H2/H3)
### Footnotes


Bibliography:
From bpt:
### bibliography: Bibliography (always use word after ':')
```bibl (bibTex)
Style this: 
Pim says: (p.71): Brill Roman 10 pt.
author: smallcaps: Familyname, initials
book/chapter/title: italics
pub-year: at the end

\newpage (new spread)
Left page:
#### Edition
Testimonia:
From bpt: textpart: T... Number as indicated.
 
from ```yaml 
* source
: 
(NOT title)
Content of Testimonium (Greek or Latin)

Footnotes: number in text, footnote at the bottom

right page:
from bpt:
#### Translation

\newpage (new spread)

Fragmenta:
From bpt: textpart: T... Number as indicated.
Title of work it is coming from:
``` yaml:
* work Mentioned/(lost work)

: 
(NOT title)
Content of Fragmenta (Greek or Latin)
Footnotes: number in text, footnote at the bottom

right page:
from bpt:
#### Translation



