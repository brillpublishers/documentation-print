Book Production for MRW
[information gathered from CDE (http://brillnet/CDE-books/central-desk-editing-books), o.a. “Procedure CDE Books Desk Editing.pdf”]
Schedule:
1.	Proof 1
Manuscript:
-	Send to typesetters (each article/lemma apart)
-	Includes Table of Contents
-	Includes prelims see CIP
-	Request .txt file of whole MS for CIP
Cover: 
-	Form: Excel program File > New > My Templates > Klopotek > Cover Request
Automatically filled in
needed: ‘long description’ (blurb) and ‘bibliographical note’ (biography) from “Content” tab in Klopotek.
In case of multiple editors, biographies can also provided in a separate document
-	Request first proof of cover (check for subsequent volumes the lay-out of first vol)
See for preferred cover designer G:\departments\Operational Process Management\Desk Editing\0Books\Cover designers book series
-	Check title and author’s name in particular
-	send proof 1 cover to assistant editor (first) + editors (after AE approved) for approval
-	request front cover jpeg + send to covers@brill.com
CIP:
-	request CIP if not yet available (for series, only for the first volume a CIP needs to be requested, subsequent volumes will be added to CIP by LOC)
-	needed: .txt file of TOC, prelims, content
-	see below for more information
Prelims:
-	Excel program File > New > My Templates > Klopotek > Prelim Copy Form
Automatically filled in from Klopotek
-	check form carefully and add additional information manually
-	p. ii : include a model page ii from most recent previous volume, mark changes to editors(‘ names) and editorial advisory board
-	p. iv : check ISSN
in case of cover illustration, credit should be put on this page

2.	Proof 2
Manuscript:
-	Receive corrections from author/editor [in CDE this takes 6 weeks, here longer?]
-	AU corrections proof 1 > Brill > to ED > ED corrections proof 1 
results in documents with both AU and ED corrections
-	Receive proof 2 from typesetters [CDE gives 2 weeks, definitely longer]
1 pdf of MS
-	Send proof 2 to assistant editor
Cover:
-	Send cover corrections
-	Show editor proof 2 if s/he requested it
CIP: 
-	Check and send to typesetters

3.	Proof 3
Manuscript:
-	Check proof 2
-	Send proof 2 to editors to check
-	Send corrections to typesetters for final print file/proof 3
Cover:
-	Last corrections
-	Spine width

	Send to Production  [check]
-	publication form (excel document filled from Klopotek)
-	? put print files on p
-	= text, cover, front cover jpeg, publication form
-	Hoeveel copies brill nodig heeft (advanced copy form)

To check in Manuscript: [these are the things CDE needs to check in the proofs]
- quality of English (if insufficient, contact assistant editor)
- page numbers (consecutively throughout volume)
- List of Contributors against names in chapters
- names on back cover against names in content
- titles in ToC against titles of chapters
- captions for illustrations and tables
- illustrations black&white or colour
- resolution of illustrations: 300 dpi for grayscale, 600 dpi for b&w line figures (minimum)
- running titles
- footnotes numbered per chapter, endnotes are not used
- remove underlining, unnecessary bold
- if there are too many blank lines
- clearly distinguishable levels of headings
- consistent use of n- or m-dashes
- italics in italicized heading should become roman
- consistent use of quotation marks (single or double); Brill style for citations/block quotations, dashes, punctuation, type of quotation marks, superscripts
- remove underlining from urls
- spaces on either side =, <, > , +, - 
- bibliography (broad check): alphabetical order, last name first, no dashes for repreat names, if it looks consistent
- hyphenation/word breaks (spot check)
- stray words at top of new pages
CIP: [information copied from “Procedure CDE Desk Editing.pdf”]
Need not be requested for yearbooks. 
Request through Mireille Pijl (Pijl@brill.com). Make a copy of the PDF file of the 1st proofs using the ISBN as file name. Check if ‘Index’ and ‘Bibliography’ are listed in TOC (will be mentioned in CIP). If not, add to TOC in the RTF file provided by the typesetters (any necessary information not present in or incorrect in the PDF can be added to or changed in the RTF file as this is used for the CIP request; however, always inform Mireille if PDF and RTF are not identical). Create folder with ISBN and last name of author/editor as folder name at G:\departments\Operational Process Management\Common\CIP requests and copy the PDF and RTF files to this folder. Send e-mail to Mireille requesting CIP for your book. You will receive (after 1-2 days) an Central Desk Editing—Desk Editing Manual Books, Version 4.2 [7-DEC-18] 9 e-mail from the Library of Congress (LoC) confirming receipt of the CIP request. CIP is sent by LoC after an average of 10 working days but it may take up to 3 months. Six weeks after receipt of the application by the LoC, a reminder may be sent; please request Mireille. In case of an urgent book (for ex. Author’s PDF), a PCN may be requested through Mireille – no files are needed; an e-mail to Mireille with the ISBN will do. If a CIP has already been requested, a PCN cannot be requested if the CIP takes too long to arrive. In that case the LCCN needs to be requested through Mireille. This number will be sent within a few days (ask Mireille to send a reminder if not received after a week) and can be added at the end of the second of the standard lines given below (highlighted LCCN is an example): The Library of Congress Cataloging-in-Publication Data is available online at http://catalog.loc.gov LC record available at http://lccn.loc.gov/2016027865 If the LCCN does not arrive in time, omit the second standard line. Check data in CIP (book title, author/editor, ISBN, series and vol. no., inclusion of ‘Index’ and ‘Bibliography’) and when correct forward to typesetters. When incorrect, mark corrections on printout of CIP (and scan to e-mail) and send to Mireille requesting a replacement CIP. No replacement file needs to be requested when a late CIP arrives

Digital Offprints: [copied from “Procedure CDE Books Desk Editing.pdf”]
If requested on the transmittal form, ask the typesetters to send watermarked and secured digital offprint files (PDF) after approval of the print file(s). The offprints should have a watermark, consisting of the line ‘For use by the Author only’ plus copyright symbol, copyright year and the name Koninklijke Brill NV, at the bottom of each page and should contain a title page and TOC. Send the offprints together with a rights letter (on BrillNet > CDE Books > Documents) to the corresponding editor upon receipt. Under ‘Document properties’ check the security settings: only Printing and Comments should be ‘Allowed’.
