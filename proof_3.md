# Proof 3

In CDE, this phase is the shortest. However, considering that only at proof 2 all articles of a MRW book have come together, this phase may take much longer for MRW.

## Manuscript, part 1

When proof 2 has been received from the typesetters, check some issues:
- prelim pages:
  * title
  * author and editor names
  * ISBN and ISSN
  * CIP 
- if all articles are included, incl. introduction and others like abbreviations list
- if all articles are indeed on the page mentioned in the Table of Contents
  * if new articles are added, or the order is rearranged, check again!
- if the date is correct (in copyright and at bottom page)
- if tables, images and citations are conform Brill style. (See blue booklet *Brill Typographic Style*)

Send the proof to the main editors, who have to check this proof. Author will not get to see this proof.
Editors can check stuff like:
- if AU and ED corrections from proof 1 are indeed corrected (spot check advised)
- hyphenation of non-English words (only line break hyphenation for English is included in conversion)
- cross-references, consistancy
- enkele regels aan begin pag etc (hoe heet dat ook alweer)
- etc. 

## Manuscript, part 2

- request proof 3 from typesetter on the basis of corrections made in proof 2 (see above)
- spot check if corrections are indeed done
- in case of hyphenation of non-English words, or adding or reducing spacing in paragraphs, other line break error may occur. 
  * ask editor in such cases to help you

These two phases may take several rounds. Editor might or might not want to see the proof several times. Please consider both the time schedule, and the quality of the book. 
(i.e.: how many rounds are necessary?)

## Manuscript, part 3

- request final proof
- check prelim pages again
- run through proof to see if you missed something
- get proof to production

## CIP

Should now been included in proof.

## Cover

Should now be final. Check everything, esp. titles and names, again. Check ISBN. 

Request pdf files of cover, _cmyk.pdf (and _uspod.pdf if POD). 

