# Example of an Letter Welcoming New Authors

Dear Professor xxx,
  
Thank you for accepting to contribute to Brill’s Encyclopedia of Buddhism the following article:   

“xxx”
with commissioned word count of xxx words  
and a tentative submission deadline of xx

In return for your contribution, the publisher offers a symbolic remuneration of xx euro, which will be paid three months after publication of the printed volume in which the article will appear. In addition, you will also be offered a considerable discount on the purchase of a copy of any Brill book for your personal library.   
 Attached, please find your author agreement. If you have any questions related to content and structure, please contact your section editor. For any administrative questions, contact me at beb@brill.com
We kindly ask you to please print out, date and sign, and return a scan of the signed agreement to me at beb@brill.com or a hardcopy of it to the following address:  

Brill’s Encyclopedia of Buddhism 
c/o Brill  
P.O. Box 9000 
2300 PA Leiden 
The Netherlands 

Please email your contribution to your section editor (cc-ing beb@brill.com) as a Word document (in a Unicode font, with no footnotes/endnotes or autoformatting), and include a PDF of the article if there are any special character or other issues that you wish to clarify. We request that you do not copy-paste text from a browser directly into the document, and that you do not use special formatting for the bibliography.  
Please also ensure that you include your name as you would like it to be spelled in publication at the beginning or end of the document. 
Additionally, if you have any copyright-free images (i.e. photos that you have taken or that you have documented permission to use) that you would like to include in your article, please send them with your article, indicating clearly where they should be placed, along with a caption and image credit.  
We thank you for your willingness to collaborate in this venture!  

With best wishes, 
....

Project Coordinator/Project Manager 

.... 

Senior Acquisitions Editor  
