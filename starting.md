# Starting a New Project

The Acquisition editor will start a new project. (S/)He will gather the editor-in-chief, and together with him/her with search for section editors. 
They will consider which articles are going to be written, by whom and when approx. the deadline will be. 

## The very start

Start making the CMS or excel in which you will keep track of things. Consider all the stages the project will get through:
* contracts
* deadline for authors
* deadline for editors (editing the articles by the authors)
* typesetting proof 1
* proofreading proof 1 by AU and ED
* proof 2
* proof 3
* to production

Also, you need:
* biographies of the authors
* the sections of the project (if applicable)
* name of the section editor (if applicable)
* if the article contains images and whether copyright is fixed
* word count
* whether articles will get a summary
* whether any language problems might occur, or translations is necessary

All these can get their own column in excell, or at least need to be documented. If you would like an example of how such an excel workflow overview looks like, please contact me at oudshoorn@brill.com

## Author Agreements

The editors will contact prospective authors and will agree with upon the scope of their article, the word count and the deadline. The deadline of course needs to be consistent with the planning the Acquisition Editor has in mind.

Please keep track of the total word count at this stage, since authors generally tend to hand in *more* words than consented. The book cannot get to massive. For example, for BEB we would like a word count of below 700.000 for the articles, so it will still fit in one volume. About 7 cm is the maximum spine width.

At this point, it is vital that author will get a clear and concise (!) style guide. This style guide needs to mention the use of different fonts for some languages, see [Things to consider](considerations.md).
If authors do not implement these fonts, getting them right in a later stage is almost impossible.

The editors need to provide you with:
* authors name
* author email address
* article name
* consented word count
* agreed deadline

Make according to these data an agreement. The format of the agreement will be given by the Acquisition Editor. 

In case of a future author remuneration (when the article is actually delivered, and after publication), fill that in in the agreement. Usually, the remuneration is relative to the consented word count. 

Attach the agreement to a formal welcoming letter. An example of that can be found [here](welcome_letter.md).

Keep track of which signed agreements are received, and *name* them so that you can find them back! E.g. Surname_firstname_agreement.pdf

Check if the author actually *signed* the agreement at page 3, many forget this. 

If an author writes several articles, please also put the name of the lemma or a designated article number in the title of the agreement. Do NOT number the agreements with "1", "2" etc., since then you do not know which article belongs to which number!
Since some articles will not go through, will get a different author or will merge with other articles, keeping track of these things is vital. 

### Reminders

After a month, send a reminder to all authors who did not send their signed agreement back. 
An example of this is: 

Dear Professor ,

Hereby a friendly reminder for the email below.
We kindly ask you to please print out, date and sign, and return a scan of the signed agreement to beb@brill.com.
If you have any questions regarding the agreement or your contribution to Brill’s Encyclopedia of Buddhism, please do not hesitate to ask.

Best wishes,
....
Project Coordinator


If after this, and perhaps another, reminder the author do not return their agreements then <to fill in>
Please note that when they do not have a signed agreement, they cannot get their 35% author discount on Brill books nor their author remuneration. 

## Author deadlines

In principle, an author delivers their article to the editor(s) of the project. Which means, that Brill does not know whether the author has done so or not. 
Please note that in the welcoming letter I added the sentence asking them to cc their article to Brill when they deliver to the editors. In this case you know if the planning is met, and what the status of your project is.
Some might not to do this in any case, but it's a start.

You could also consider sending an update to the editors when the deadline of some of their authors has passed recently, to ask the editors which articles were delivered and which will arrive later. 
Emend your planning if necessary. 
Examples:
* LGGA: here I send authors a friendly reminder of their upcoming deadlines per month. (Since the editors also deliver to Brill per month a number of articles)
* SGG: here I email the editors when authors should have delivered their article(s), which happens approx. twice a year


