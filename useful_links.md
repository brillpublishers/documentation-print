[All documentation from CDE](http://brillnet/CDE-books/central-desk-editing-books)

[Procedure from CDE](http://brillnet/sites/default/files/departments/central%20desk%20editing/Books/Procedure%20CDE%20Books%20Desk%20Editing.pdf)

! [Deadlines for delivery to production](http://brillnet/sites/default/files/departments/central%20desk%20editing/Books/2019%20Deadlines%20for%20Delivery%20Production%20Files%20to%20Leiden%20Production%20Department.pdf)

[Information for authors](https://brill.com/page/PublishingBookwithBrill/publishing-books-with-brill); Please note that some of the information in this booklet is not applicable to MRW, or has to be changed. 

[Book Documents](http://brillnet/book-documents); standard trim sizes and spine width calculators