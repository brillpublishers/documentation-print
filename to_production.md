# Delivery to Production

When all corrections are done and the book is complete, you can send the files to production for printing.

Check the deadlines for delivery to production [here](http://brillnet/sites/default/files/departments/central%20desk%20editing/Books/2019%20Deadlines%20for%20Delivery%20Production%20Files%20to%20Leiden%20Production%20Department.pdf).
The second column is the deadline, the green column indicates the publication date. Please note Offset and POD books have different schedules.

## Files needed

- a print file of the book
  * titled isbn_print_content_text_pdf
  * without crop marks
- the cover(s) in pdf
  * titled isbn_print_cover_cmyk.pdf for offset and POD
  * for POD American print: isbn_print_cover_uspod.pdf
- a cover image
  * titled a number, but to which refers this? nog uitzoeken
- Publication Form 
  * found at: Excel: File > New > My Templates > Klopotek > Publication Form
  * fill in all necessary details
  * you only have to fill in the first tab
- a signed calculation
  * can be requested by the Assistant Editor, should also be in the map at G:

## Sending

Put all files in a separate folder called "Print_files", preferable in the topmost folder of the project.

Send the link to this print folder to the Production Manager and the Assistant Editor. 

## Final stuff

Of course, it is decent to inform the editors of the project that it is in production, and when the publication is scheduled.

Furthermore, request watermarked, digital offprints for each article at the typesetters. These are to be send to the authors after publication, for private use only.