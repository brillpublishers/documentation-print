# Things to consider

## Fonts

Of course, for Latin script and Greek the Brill font is used.
For other scripts, such as Chinese, Japanese and Korean, specific fonts are advised.
Authors need to know this at the preliminary stages of their writing, in later stages it cannot be corrected anymore.
See the [Brill Wiki](http://wiki/index.php?title=Special%3ASearch&search=typesetting&go=Go). 

Please also note that if the print book will be published online in later stages, the languages are to be marked in xml (xml-lang:Greek, for instance). 
For correct conversion to xml for online, using the correct fonts is thus crucial as well. 

## Brill Style

Brill has a specific typographic style, explained in the blue booklet *Brill Typographic Style* (BTS).
Most typesetters we work with regularly know this style, but please check if tables and citations are correct.

Besides, please check if AU or ED corrections do not go against the BTS. This is especially important in critical editions, since several styles for marking certain text critical forms are used.
For example, German critical editions often used character spacing to indicate emphasis on a specific part of the text. BTS forbids character spacing, instead use bold type or bold+italics. 

Furthermore, please check in critical editions if < and >, a common marking for editorial notes are replaced in typesetting by ⟨ and ⟩ ,Unicode 27E8 and 27E9.


## Bibliography

Also has a specific style. Check Chicago (login can be provided, email me) and BTS.

Please note:

For Western authors: Surname, Initial. 
  * e.g. Jansen, J.

For Asian authors, writing in Western language: Surname, Personal Name
  * e.g. Wang, Beixing
  
For Asian authors, writing in Asian (own) language: Surname Personal Name
  * e.g. Wang Beixing
  
I know, confusing. 

Also check: In (critical) text editions, abbreviations of certain oft-used sources are frequent. 
In the bibliography, these are either to be avoided, ór should be explained in a list in the prelims. 

## Keeping on top of it
MRW projects are huge, dealing with many authors and several editors. Making sure you keep track of things is essential. 
Consider using CMS (and hów are you going to use it?), or - as I do - use an excel for your workflow. Use 1 column for each of your workflow stages, and regularly check the status of your project.
Documenting stuff correctly, asap, and in order that you can find it back is absolutely key to this projects. 

Also, keep in mind that when you email the authors (in case of contracts, proofreading, or sending the digital offprints) they will <i>all</i> email you back at the same time causing a huge influx of emails.
They will also expect an answer sooner than you can give it. Please make sure that in your emails you mention in which period they can expect an answer/whatever it is you are telling them in the email.

### Mail merge

When you need to send a considerable number of emails - in case of contracts, proofreading, or digital offprints - you could make use of [Mail Merge](https://support.office.com/en-us/article/mail-merge-using-an-excel-spreadsheet-858c7d7f-5cc0-4ba1-9a7b-0a948fa3d7d3).
Please note that in order for this to go well you need a very orderly and up-to-date excel list of the people you need to email, and the information you need to email them. 
Also, you should have marked exceptions clearly, so you can filter those out easily and email them separately. 
Please note: you cannot add attachments to a mail merge generated email. If you would like to do so, click finish merg > edit document. Then, copypaste each email to a outlook and attach necessary documents. Bit more work, admittedly.
